<?php

declare(strict_types=1);

namespace Smolowik\Propel\Behavior;

use Propel\Generator\Builder\Om\ObjectBuilder;
use Propel\Generator\Model\Table;
use Propel\Generator\Util\PhpParser;

class MysqlEncryptionBehaviorObjectBuilderModifier
{
    /**
     * @var MysqlEncryptionBehavior
     */
    protected $behavior;

    /**
     * @var Table
     */
    protected $table;

    /**
     * @param MysqlEncryptionBehavior $behavior
     */
    public function __construct(MysqlEncryptionBehavior $behavior)
    {
        $this->behavior = $behavior;
        $this->table = $behavior->getTable();
    }

    public function objectFilter(string &$script, ObjectBuilder $builder)
    {
        $parser = new PhpParser($script, true);
        $doInsertFunction = $parser->findMethod('doInsert');
        $buildCriteriaFunction = $parser->findMethod('buildCriteria');
        $defineVariable = "\$modifiedColumnsExpression = [];\r\n        \$index = 0;";
        $doInsertFunction = str_replace('$index = 0;', $defineVariable, $doInsertFunction);
        $builder->declareClass('Smolowik\\Propel\\Passphrase');

        $columns = explode(',', $this->behavior->getParameter('columns'));
        foreach ($this->table->getColumns() as $column) {
            $modifiedColumnsExpression = sprintf(
                "\$modifiedColumnsExpression[] = ':p' . \$index;\r\n            \$modifiedColumns[':p' . \$index++]  = '%s';",
                $column->getName()
            );
            if (in_array($column->getName(), $columns, true)) {
                $modifiedColumnsExpression = sprintf(
                    "\$modifiedColumnsExpression[] = 'AES_ENCRYPT(%s, UNHEX(SHA2(\\'' . %s . '\\',512)))';\r\n            \$modifiedColumns[':p' . \$index++]  = '%s';",
                    ":p' . \$index . '",
                    'Passphrase::getInstance()->getPassphrase()',
                    $column->getName()
                );

                $buildCriteriaFunction = str_replace(
                    sprintf(
                        '$this->%s',
                        $column->getName()
                    ).')',
                    sprintf(
                        /* TSN */
                        "'AES_ENCRYPT(\'' . addslashes(\$this->%s) . '\', UNHEX(SHA2(\'' . %s . '\',512)))', Criteria::CUSTOM_EQUAL",
                        /* / TSN */
                        $column->getName(),
                        'Passphrase::getInstance()->getPassphrase()'
                    ).')',
                    $buildCriteriaFunction
                );
            }

            $doInsertFunction = str_replace(
                sprintf(
                    '$modifiedColumns[\':p\' . $index++]  = \'%s\';',
                    $column->getName()
                ),
                $modifiedColumnsExpression,
                $doInsertFunction
            );
        }
        $doInsertFunction = str_replace(
            'implode(\', \', array_keys($modifiedColumns))',
            'implode(\', \', $modifiedColumnsExpression)',
            $doInsertFunction
        );
        $parser->replaceMethod('doInsert', $doInsertFunction);
        $parser->replaceMethod('buildCriteria', $buildCriteriaFunction);
        $script = $parser->getCode();

        /* TSN */
        $columns = explode(',', $this->behavior->getParameter('columns'));
        foreach ($this->table->getColumns() as $column) {
            if (in_array($column->getName(), $columns, true)) {
                $this->modifyGetter($script, $column);
                $this->modifySetter($script, $column, $parser);
                $this->modifyHydrator($script, $column, $parser);
            }
        }
        /* / TSN */

    }

    /* TSN */
    protected function modifyGetter(&$script, &$column)
    {
        $getterLocation = strpos($script, "get".$column->getPhpName());
        $start = strpos($script, "return", $getterLocation) + 7;
        $length = strpos($script, ";", $getterLocation) - $start;
        $variableName = substr($script, $start, $length);
        $insertionStart = strpos($script, "return", $getterLocation);
        $insertionLength = strpos($script, ";", $insertionStart) - $insertionStart + 1;

        $content = <<<EOT
// TSN: Encrypted column
        \$fieldValue = $variableName;
        if (is_resource(\$fieldValue) && get_resource_type(\$fieldValue) === "stream") {
            \$fieldValue = stream_get_contents(\$fieldValue, -1, 0);
        }
        return \$fieldValue;
EOT;

        $script = substr_replace($script, $content, $insertionStart, $insertionLength);
    }

    protected function modifySetter(&$script, &$column, &$parser)
    {
        $methodName = "set".$column->getPhpName();
        $methodString = $parser->findMethod($methodName);

        $find = 'set'.$column->getPhpName().'($v)';
        $start = strpos($methodString, $find);

        $toReplace = substr($methodString, $start + strlen($find));
        $toReplace = substr($toReplace, strpos($toReplace, '{')+1);

        $methodString = substr($methodString, 0, $start + strlen($find));

        $content = <<<EOT
    {
        // TSN: Encrypted column
        if (\$v !== null) {
            \$v = (string) \$v;
        }

        if (\$this->{$column->getName()} !== \$v) {
            \$this->{$column->getName()} = \$v;
            \$this->modifiedColumns[{$column->getTable()->getPhpName()}TableMap::COL_{$column->getUppercasedName()}] = true;
        }

        return \$this;
    }
EOT;
        $methodString = $methodString.PHP_EOL.$content;
        $parser->replaceMethod($methodName, $methodString);
        $script = $parser->getCode();
    }

    protected function modifyHydrator(&$script, &$column, &$parser)
    {
        $hydrateMethod = $parser->findMethod('hydrate');
        $startColumn = strpos($hydrateMethod, $column->getPhpName());
        $string = substr($hydrateMethod, $startColumn);

        $end = strpos($string, '$col = $row[TableMap::');
        if ($end === false) {
            $end = strpos($string, '$this->resetModified();');
        }
        $string = substr($string, 0, $end);

        $endString = ')];';
        $start2 = strpos($string, $endString);
        $string = substr($string, $start2 + strlen($endString));

        $newString = PHP_EOL."\t\t\t".'// TSN: Encrypted column'.PHP_EOL."\t\t\t".'$this->'.$column->getName().' = (null !== $col) ? (string) $col : null;'.PHP_EOL.PHP_EOL."\t\t\t";

        $script = str_replace($string, $newString, $script);
    }
    /* / TSN */
}
